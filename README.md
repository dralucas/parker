# PARKER --- Planetary lidAR seeKing for lifE signatuRe

## Introducing the “Feature Discovery from Massive Planetary LiDAR Data (FARMYARD)” framework

### Antoine Lucas, Shen Liang, Stephen Porder, Gregory Sainton, Alexandre Fournier,Eric Gayer

#### Motivations

Topography, the surface configuration of a planet, bears the imprint of the processes that shape it. Deciphering the "text" encoded in topography has been the goal of scholars for centuries (Darwin, Gilbert, others). For scholars looking into space, topography is a window in the processes that shape alien worlds. Here on Earth, it offers clues to how our planet might be different, and if so why. Today, with the revolution in Earth observations from space, we can approach this question with fresh eyes and extend it to other bodies of the Solar System. Doing so requires the interpretation of a wide range of data sets, and cutting edge mathematical approaches to their integration. This integration and interpretation are the focus of this proposal.


![](Parker_poster_gitlab.png) 


#### References: 

[1]  D.  E.  Smith,  M.  T.  Zuber,  S.  C.  Solomon,  R.  J.  Phillips,  J.  W.  Head,J. B. Garvin, W. B. Banerdt, D. O. Muhleman, G. H. Pettengill, G. A.Neumannet  al.,  “The  global  topography  of  mars  and  implications  forsurface evolution,”Science, vol. 284, no. 5419, pp. 1495–1503, 1999.

[2]  B.  W.  Stiles,  S.  Hensley,  Y.  Gim,  D.  M.  Bates,  R.  L.  Kirk,  A.  Hayes,J.  Radebaugh,  R.  D.  Lorenz,  K.  L.  Mitchell,  P.  S.  Callahanet  al.,“Determining  titan  surface  topography  from  cassini  sar  data,”Icarus,vol. 202, no. 2, pp. 584–598, 2009.

[3]  D. E. Smith, M. T. Zuber, G. A. Neumann, F. G. Lemoine, E. Mazarico,M.  H.  Torrence,  J.  F.  McGarry,  D.  D.  Rowlands,  J.  W.  Head,  T.  H.Duxburyet al., “Initial observations from the lunar orbiter laser altimeter(lola),”Geophysical Research Letters, vol. 37, no. 18, 2010.

[4]  M.  A.  Wieczorek,  “Gravity  and  topography  of  the  terrestrial  planets,”Treatise on Geophysics, vol. 10, pp. 165–206, 2007.
[5]  R. D. Lorenz, B. W. Stiles, O. Aharonson, A. Lucas, A. G. Hayes, R. L.Kirk,  H.  A.  Zebker,  E.  P.  Turtle,  C.  D.  Neish,  E.  R.  Stofanet  al.,  “Aglobal topographic map of titan,”Icarus, vol. 225, no. 1, pp. 367–377,2013.

[6]  C. S. Balzotti and G. P. Asner, “Episodic canopy structural transforma-tions  and  biological  invasion  in  a  hawaiian  forest,”Frontiers  in  plantscience, vol. 8, p. 1256, 2017.

[7]  C. Mallet and F. Bretar, “Full-waveform topographic lidar: State-of-the-art,”ISPRS  Journal  of  photogrammetry  and  remote  sensing,  vol.  64,no. 1, pp. 1–16, 2009.

[8]  E.  Mazarico,  D.  Rowlands,  G.  Neumann,  D.  Smith,  M.  Torrence,F.  Lemoine,  and  M.  Zuber,  “Orbit  determination  of  the  lunar  recon-naissance  orbiter,”Journal  of  Geodesy,  vol.  86,  no.  3,  pp.  193–207,2012.

[9]  D. G. Krige, “A statistical approach to some basic mine valuation prob-lems on the witwatersrand,”Journal of the Southern African Institute ofMining and Metallurgy, vol. 52, no. 6, pp. 119–139, 1951.

[10]  D.  Shepard,  “A  two-dimensional  interpolation  function  for  irregularly-spaced  data,”  inProceedings  of  the  1968  23rd  ACM  National  Confer-ence, 1968, pp. 517–524.

[11]  NVIDIA, “Cuda c++ programming guide,” 2021.

[12]  N.  Brodu  and  D.  Lague,  “3d  terrestrial  lidar  data  classification  ofcomplex  natural  scenes  using  a  multi-scale  dimensionality  criterion:Applications in geomorphology,”ISPRS Journal of Photogrammetry andRemote Sensing, vol. 68, pp. 121–134, 2012.
